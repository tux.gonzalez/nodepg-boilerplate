const express = require('express');
const cors = require('cors');
const routerApi = require('./routes');

const port = process.env.PORT || 3030;
const {
  errorHandler,
  boomErrorHandler,
  ormErrorHandler,
  logErrors
} = require('./middlewares/error.handler');

const app = express();
app.use(express.json());
const whitelist = ['http://localhost:8080', 'https://myapp.co'];
const options = {
  origin: (origin, callback) => {
    if (whitelist.includes(origin) || !origin) {
      callback(null, true);
    } else {
      callback(new Error('no permitido'));
    }
  }
}
app.use(cors(options));

routerApi(app);


app.use(logErrors)
app.use(ormErrorHandler);
app.use(boomErrorHandler);
app.use(errorHandler)

app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`Running in port: ${port}`)
})
